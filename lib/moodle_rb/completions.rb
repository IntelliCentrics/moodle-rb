module MoodleRb
  class Completions
    include HTTParty
    include Utility

    attr_reader :token, :query_options

    def initialize(token, url, query_options)
      @token = token
      @query_options = query_options
      self.class.base_uri url
    end

    # required params:
    # userid
    # courseid
    def course_completion_status(params)
      response = self.class.post(
        '/webservice/rest/server.php',
        {
          :query => query_hash('core_completion_get_course_completion_status', token),
          :body => {
            :userid => params[:userid],
            :courseid => params[:courseid]
          }
        }.merge(query_options)
      )
      check_for_errors(response)
      response.parsed_response
    end

  end
end
